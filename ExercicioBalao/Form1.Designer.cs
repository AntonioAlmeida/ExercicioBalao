﻿namespace ExercicioBalao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelCorBalao = new System.Windows.Forms.Label();
            this.LabelDirecao = new System.Windows.Forms.Label();
            this.LabelAltura = new System.Windows.Forms.Label();
            this.LabelMensagem = new System.Windows.Forms.Label();
            this.BtnNorte = new System.Windows.Forms.Button();
            this.BtnOeste = new System.Windows.Forms.Button();
            this.BtnEste = new System.Windows.Forms.Button();
            this.BtnSul = new System.Windows.Forms.Button();
            this.BtnAumentaAltura = new System.Windows.Forms.Button();
            this.BtnDiminuiAltura = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.BtnSelectCor = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelAltura);
            this.groupBox1.Controls.Add(this.LabelDirecao);
            this.groupBox1.Controls.Add(this.LabelCorBalao);
            this.groupBox1.Location = new System.Drawing.Point(56, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 294);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Balao";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // LabelCorBalao
            // 
            this.LabelCorBalao.AutoSize = true;
            this.LabelCorBalao.Location = new System.Drawing.Point(26, 61);
            this.LabelCorBalao.Name = "LabelCorBalao";
            this.LabelCorBalao.Size = new System.Drawing.Size(23, 13);
            this.LabelCorBalao.TabIndex = 0;
            this.LabelCorBalao.Text = "Cor";
            this.LabelCorBalao.Click += new System.EventHandler(this.LabelCorBalao_Click);
            // 
            // LabelDirecao
            // 
            this.LabelDirecao.AutoSize = true;
            this.LabelDirecao.Location = new System.Drawing.Point(26, 136);
            this.LabelDirecao.Name = "LabelDirecao";
            this.LabelDirecao.Size = new System.Drawing.Size(50, 13);
            this.LabelDirecao.TabIndex = 1;
            this.LabelDirecao.Text = "Direcção";
            // 
            // LabelAltura
            // 
            this.LabelAltura.AutoSize = true;
            this.LabelAltura.Location = new System.Drawing.Point(26, 256);
            this.LabelAltura.Name = "LabelAltura";
            this.LabelAltura.Size = new System.Drawing.Size(34, 13);
            this.LabelAltura.TabIndex = 2;
            this.LabelAltura.Text = "Altura";
            // 
            // LabelMensagem
            // 
            this.LabelMensagem.AutoSize = true;
            this.LabelMensagem.Location = new System.Drawing.Point(268, 9);
            this.LabelMensagem.Name = "LabelMensagem";
            this.LabelMensagem.Size = new System.Drawing.Size(59, 13);
            this.LabelMensagem.TabIndex = 3;
            this.LabelMensagem.Text = "Mensagem";
            // 
            // BtnNorte
            // 
            this.BtnNorte.Location = new System.Drawing.Point(252, 96);
            this.BtnNorte.Name = "BtnNorte";
            this.BtnNorte.Size = new System.Drawing.Size(75, 23);
            this.BtnNorte.TabIndex = 1;
            this.BtnNorte.Text = "Norte";
            this.BtnNorte.UseVisualStyleBackColor = true;
            this.BtnNorte.Click += new System.EventHandler(this.BtnNorte_Click);
            // 
            // BtnOeste
            // 
            this.BtnOeste.Location = new System.Drawing.Point(197, 125);
            this.BtnOeste.Name = "BtnOeste";
            this.BtnOeste.Size = new System.Drawing.Size(75, 23);
            this.BtnOeste.TabIndex = 2;
            this.BtnOeste.Text = "Oeste";
            this.BtnOeste.UseVisualStyleBackColor = true;
            this.BtnOeste.Click += new System.EventHandler(this.BtnOeste_Click);
            // 
            // BtnEste
            // 
            this.BtnEste.Location = new System.Drawing.Point(302, 125);
            this.BtnEste.Name = "BtnEste";
            this.BtnEste.Size = new System.Drawing.Size(75, 23);
            this.BtnEste.TabIndex = 3;
            this.BtnEste.Text = "Este";
            this.BtnEste.UseVisualStyleBackColor = true;
            this.BtnEste.Click += new System.EventHandler(this.BtnEste_Click);
            // 
            // BtnSul
            // 
            this.BtnSul.Location = new System.Drawing.Point(252, 154);
            this.BtnSul.Name = "BtnSul";
            this.BtnSul.Size = new System.Drawing.Size(75, 23);
            this.BtnSul.TabIndex = 4;
            this.BtnSul.Text = "Sul";
            this.BtnSul.UseVisualStyleBackColor = true;
            this.BtnSul.Click += new System.EventHandler(this.BtnSul_Click);
            // 
            // BtnAumentaAltura
            // 
            this.BtnAumentaAltura.Location = new System.Drawing.Point(221, 247);
            this.BtnAumentaAltura.Name = "BtnAumentaAltura";
            this.BtnAumentaAltura.Size = new System.Drawing.Size(75, 23);
            this.BtnAumentaAltura.TabIndex = 5;
            this.BtnAumentaAltura.Text = "+";
            this.BtnAumentaAltura.UseVisualStyleBackColor = true;
            this.BtnAumentaAltura.Click += new System.EventHandler(this.BtnAumentaAltura_Click);
            // 
            // BtnDiminuiAltura
            // 
            this.BtnDiminuiAltura.Location = new System.Drawing.Point(324, 247);
            this.BtnDiminuiAltura.Name = "BtnDiminuiAltura";
            this.BtnDiminuiAltura.Size = new System.Drawing.Size(75, 23);
            this.BtnDiminuiAltura.TabIndex = 6;
            this.BtnDiminuiAltura.Text = "-";
            this.BtnDiminuiAltura.UseVisualStyleBackColor = true;
            this.BtnDiminuiAltura.Click += new System.EventHandler(this.BtnDiminuiAltura_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Branco",
            "Vermelho",
            "Laranja",
            "Amarelo",
            "Verde",
            "Azul",
            "Indigo",
            "Violeta",
            "Preto"});
            this.comboBox1.Location = new System.Drawing.Point(183, 57);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 7;
            // 
            // BtnSelectCor
            // 
            this.BtnSelectCor.Location = new System.Drawing.Point(302, 55);
            this.BtnSelectCor.Name = "BtnSelectCor";
            this.BtnSelectCor.Size = new System.Drawing.Size(126, 23);
            this.BtnSelectCor.TabIndex = 8;
            this.BtnSelectCor.Text = "Seleciona Cor";
            this.BtnSelectCor.UseVisualStyleBackColor = true;
            this.BtnSelectCor.Click += new System.EventHandler(this.BtnSelectCor_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 294);
            this.Controls.Add(this.BtnSelectCor);
            this.Controls.Add(this.LabelMensagem);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.BtnDiminuiAltura);
            this.Controls.Add(this.BtnAumentaAltura);
            this.Controls.Add(this.BtnSul);
            this.Controls.Add(this.BtnEste);
            this.Controls.Add(this.BtnOeste);
            this.Controls.Add(this.BtnNorte);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LabelAltura;
        private System.Windows.Forms.Label LabelDirecao;
        private System.Windows.Forms.Label LabelCorBalao;
        private System.Windows.Forms.Label LabelMensagem;
        private System.Windows.Forms.Button BtnNorte;
        private System.Windows.Forms.Button BtnOeste;
        private System.Windows.Forms.Button BtnEste;
        private System.Windows.Forms.Button BtnSul;
        private System.Windows.Forms.Button BtnAumentaAltura;
        private System.Windows.Forms.Button BtnDiminuiAltura;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button BtnSelectCor;
    }
}

