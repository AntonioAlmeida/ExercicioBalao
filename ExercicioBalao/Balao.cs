﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioBalao
{
   public class Balao
    {

        #region Atributos

        private string _cor;

        private string _direcao;

        private int _altura;

        private string _mensagem;

        #endregion

        #region Propriedades

        public string Cor { get; set; }
        public string Direcao { get; set; }

        public int Altura { get; set; }

        public string Mensagem { get; set; }

        #endregion

        #region Construtores

        public Balao()
        {
            Cor = "Branco";
            Direcao = "Norte";
            Altura = 1;
            Mensagem = "Novo Balão";

        }



        public Balao(string cor,string direcao,int altura)
        {
            Cor = "Branco";
            Direcao = "Norte";
            Altura = 1;
            Mensagem = "Novo Balão";

        }

        #endregion

        public void AumentaAltura(int valor)
        {

            Altura = Altura + 1;

        }


        public void DiminuiAltura(int valor)
        {

            if (Altura > 0)
            {
                Altura = Altura - 1;
            }else
            {
                Mensagem = "O balão já está no solo";
            }
        }

    }
}
