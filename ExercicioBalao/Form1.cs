﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExercicioBalao
{
    public partial class Form1 : Form
    {
        private Balao meuBalao;
        public Form1()
        {
            InitializeComponent();
            meuBalao = new Balao();

            LabelCorBalao.Text = meuBalao.Cor;
            LabelDirecao.Text = meuBalao.Direcao;
            LabelAltura.Text = meuBalao.Altura.ToString();
            LabelMensagem.Text = "Balão";

        }

        private void LabelCorBalao_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnSelectCor_Click(object sender, EventArgs e)
        {
            meuBalao.Cor = comboBox1.Text;
            LabelCorBalao.Text = meuBalao.Cor;
            comboBox1.Text = "";
            meuBalao.Mensagem = "Criou um Novo Balão";
            LabelMensagem.Text = meuBalao.Mensagem;
        }

        private void BtnNorte_Click(object sender, EventArgs e)
        {
            LabelDirecao.Text = BtnNorte.Text;
            BtnNorte.Enabled = false;
            BtnOeste.Enabled = true;
            BtnSul.Enabled = true;
            BtnEste.Enabled = true;
            meuBalao.Mensagem = "Balão";
            LabelMensagem.Text = meuBalao.Mensagem;
        }

        private void BtnOeste_Click(object sender, EventArgs e)
        {
            LabelDirecao.Text = BtnOeste.Text;
            BtnOeste.Enabled = false;
            BtnNorte.Enabled = true;
            BtnSul.Enabled = true;
            BtnEste.Enabled = true;
            meuBalao.Mensagem = "Balão";
            LabelMensagem.Text = meuBalao.Mensagem;
        }

        private void BtnSul_Click(object sender, EventArgs e)
        {
            LabelDirecao.Text = BtnSul.Text;
            BtnSul.Enabled = false;
            BtnNorte.Enabled = true;
            BtnOeste.Enabled = true;
            BtnEste.Enabled = true;
            meuBalao.Mensagem = "Balão";
            LabelMensagem.Text = meuBalao.Mensagem;

        }

        private void BtnEste_Click(object sender, EventArgs e)
        {
            LabelDirecao.Text = BtnEste.Text;
            BtnEste.Enabled = false;
            BtnNorte.Enabled = true;
            BtnOeste.Enabled = true;
            BtnSul.Enabled = true;
            meuBalao.Mensagem = "Balão";
            LabelMensagem.Text = meuBalao.Mensagem;
        }

        private void BtnAumentaAltura_Click(object sender, EventArgs e)
        {
            int valor = 1;
            meuBalao.AumentaAltura(valor);
            LabelAltura.Text = meuBalao.Altura.ToString();
            meuBalao.Mensagem = "Balão";
            LabelMensagem.Text = meuBalao.Mensagem;
        }

        private void BtnDiminuiAltura_Click(object sender, EventArgs e)
        {


            int valor = 1;
            meuBalao.DiminuiAltura(valor);
            LabelAltura.Text = meuBalao.Altura.ToString();
            LabelMensagem.Text = meuBalao.Mensagem;

        }
    }
}
